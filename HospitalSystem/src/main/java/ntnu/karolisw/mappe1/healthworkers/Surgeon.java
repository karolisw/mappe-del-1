package ntnu.karolisw.mappe1.healthworkers;

import ntnu.karolisw.mappe1.hospital.Patient;

/**
 * Class Surgeon
 * @author Karoline Sund Wahl
 * @version 1 (05.03.2021)
 */
public class Surgeon extends Doctor{


    /**
     * Constructor calls on super(Doctor) such as to not write excessive code
     * @param firstname
     * @param lastname
     * @param socialSecurityNumber
     */
    public Surgeon(String firstname, String lastname, String socialSecurityNumber) {
        super(firstname, lastname, socialSecurityNumber);
    }

    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }
}

package ntnu.karolisw.mappe1.healthworkers;

import ntnu.karolisw.mappe1.hospital.Employee;

/**
 * Class Nurse
 * @author Karoline Sund Wahl
 * @version 1 (05.03.2021)
 */
public class Nurse extends Employee {

    /**
     * Constructer calling on super(Employee). This health worker cannot set diagnosis
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Nurse(String firstName, String lastName, String socialSecurityNumber){
        super(firstName,lastName,socialSecurityNumber);
    }

    /**
     * ToString()-method calling on super such as to not write unnecessary code
     * @return String
     */
    @Override
    public String toString() {
        return super.toString();
    }
}

package ntnu.karolisw.mappe1.healthworkers;

import ntnu.karolisw.mappe1.hospital.Patient;

/**
 * Class GeneralPractitioner
 * @author Karoline Sund Wahl
 * @version 1 (05.03.2021)
 */
public class GeneralPractitioner extends Doctor{

    /**
     * Constructor calls on super(Doctor)
     * @param firstname
     * @param lastname
     * @param socialSecurityNumber
     */
    public GeneralPractitioner(String firstname, String lastname, String socialSecurityNumber) {
        super(firstname, lastname, socialSecurityNumber);
    }

    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }
}

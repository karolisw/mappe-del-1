package ntnu.karolisw.mappe1.healthworkers;

import ntnu.karolisw.mappe1.hospital.Employee;
import ntnu.karolisw.mappe1.hospital.Patient;

/**
 * Abstract class Doctor
 * @author Karoline Sund Wahl
 * @version 1 (05.03.2021)
 */

public abstract class Doctor extends Employee {

    /**
     * Constructor with protected access
     * Extends constructor in class Employee
     * @param firstname
     * @param lastname
     * @param socialSecurityNumber
     */
    protected Doctor(String firstname, String lastname, String socialSecurityNumber){
        super(firstname, lastname, socialSecurityNumber);
    }

    /**
     * Only GeneralPractitioner -and Surgeon objects can set a patients diagnosis
     * @param patient
     * @param diagnosis
     */
    public abstract void setDiagnosis(Patient patient, String diagnosis);
}

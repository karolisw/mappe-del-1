package ntnu.karolisw.mappe1.exceptions;

/**
 * Custom Exception-cLass.
 * Used in Department-class removePerson()-method.
 * Might occur if Person to be removed is not Person, or if Person does not exist in either ArrayLists.
 * Methods calling on removePerson()-method will have try/catch block to catch this exception when thrown from method.
 * RemoveException is a checked exception. Therefore, it extends Exception.
 *
 *
 * Class RemoveException
 * @author Karoline Sund Wahl
 * @version 1.1 (11.03.2021)
 */

public class RemoveException extends Exception{
    private static final long serialVersionUid = 1L;

    public RemoveException(String message){
        super(message);
    }


}

package ntnu.karolisw.mappe1.hospital;

import ntnu.karolisw.mappe1.exceptions.RemoveException;
import ntnu.karolisw.mappe1.healthworkers.GeneralPractitioner;
import ntnu.karolisw.mappe1.healthworkers.Nurse;
import ntnu.karolisw.mappe1.healthworkers.Surgeon;
import java.util.ArrayList;

/**
 * Client HospitalClient
 * @author Karoline Sund Wahl
 * @version 1 (05.03.2021)
 */
public final class HospitalClient {


    public static void fillRegisterWithTestData(final Hospital hospital) {
        // Add some departments
        Department emergency = new Department("Akutten");
        emergency.getEmployees().add(new Employee("Odd Even", "Primtallet", ""));
        emergency.getEmployees().add(new Employee("Huppasahn", "DelFinito", ""));
        emergency.getEmployees().add(new Employee("Rigmor", "Mortis", ""));
        emergency.getEmployees().add(new GeneralPractitioner("Inco", "Gnito", ""));
        emergency.getEmployees().add(new Surgeon("Inco", "Gnito", ""));
        emergency.getEmployees().add(new Nurse("Nina", "Teknologi", ""));
        emergency.getEmployees().add(new Nurse("Ove", "Ralt", ""));
        Nurse nurse = new Nurse("Liss", "Moss", "1234");
        emergency.getEmployees().add(nurse);
        emergency.getPatients().add(new Patient("Inga", "Lykke", ""));
        emergency.getPatients().add(new Patient("Ulrik", "Smål", ""));
        hospital.getDepartments().add(emergency);

        Department childrenPolyclinic = new Department("Barn poliklinikk");
        childrenPolyclinic.getEmployees().add(new Employee("Salti", "Kaffen", ""));
        childrenPolyclinic.getEmployees().add(new Employee("Nidel V.", "Elvefølger", ""));
        childrenPolyclinic.getEmployees().add(new Employee("Anton", "Nym", ""));
        childrenPolyclinic.getEmployees().add(new GeneralPractitioner("Gene", "Sis", ""));
        childrenPolyclinic.getEmployees().add(new Surgeon("Nanna", "Na", ""));
        childrenPolyclinic.getEmployees().add(new Nurse("Nora", "Toriet", ""));
        childrenPolyclinic.getPatients().add(new Patient("Hans", "Omvar", ""));
        childrenPolyclinic.getPatients().add(new Patient("Laila", "La", ""));
        childrenPolyclinic.getPatients().add(new Patient("Jøran", "Drebli", ""));
        hospital.getDepartments().add(childrenPolyclinic);
    }

    public static void main(String[] args) throws RemoveException {
        //Making HospitalObject to add data to
        Hospital stOlav = new Hospital("St. Olavs Hospital");
        fillRegisterWithTestData(stOlav);

        /**
         * Creating nested for-each loop to test removePerson()- method
         * ArrayList selectedEmployees consisting of all Nurse-objects in hospital stOlav
         * RemovePerson()-method is used to remove selectedEmployees from stOlav
         */

        for(Department department : stOlav.getDepartments()){
            ArrayList<Employee> selectedEmployees = new ArrayList<>();
            for(Employee employee : department.getEmployees()){
                if(employee instanceof Nurse){
                    selectedEmployees.add(employee);
                }
            }
            for(Employee nurse : selectedEmployees){
                department.removePerson(nurse);
            }
        }
        System.out.println(stOlav.getDepartments());


        /**
         * Testing call on removePerson()-method on non-existing Patient object
         */
        for(Department department : stOlav.getDepartments()){
            Patient nonExistingPatient = new Patient("Post","Malone","123456");
            try{
                department.removePerson(nonExistingPatient);
            }catch (RemoveException e){
                System.out.println("Exception message: " + e.getMessage() );
            }
        }
        System.out.println(stOlav);
    }

}

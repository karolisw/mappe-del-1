package ntnu.karolisw.mappe1.hospital;

/**
 * Class Patient
 * @author Karoline Sund Wahl
 * @version 1 (05.03.2021)
 */
public class Patient extends Person implements Diagnosable{
    private String diagnosis;

    /**
     * Constructor is protected and not accessible for classes outside of hospital package
     * Diagnosis is empty String until set by a subclass of Doctor
     * Calls on super(Person) to avoid code replication
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
        diagnosis = "";
    }

    protected String getDiagnosis(){
        return diagnosis;
    }

    /**
     * Mutator with simple argument check
     * @throws IllegalArgumentException
     * @param diagnosis
     */
    public void setDiagnosis(String diagnosis) {
        if(diagnosis == null){
            throw new IllegalArgumentException();
        }
        this.diagnosis = diagnosis;
    }

    /**
     * toString()-method calls on super to avoid code replication
     * Diagnosis is not printed and is only available through getters.
     * @return String
     */
    @Override
    public String toString() {
        return super.toString();
    }
}

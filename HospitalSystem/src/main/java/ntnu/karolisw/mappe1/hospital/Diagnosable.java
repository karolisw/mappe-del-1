package ntnu.karolisw.mappe1.hospital;

/**
 * Interface Diagnosable
 * @author Karoline Sund Wahl
 * @version 1 (05.03.2021)
 */
public interface Diagnosable {

    /**
     * Interface-class. Implemented by Doctor and subclasses.
     * Method to set the diagnosis of a patients
     * @param diagnosis
     */
    void setDiagnosis(String diagnosis);
}

package ntnu.karolisw.mappe1.hospital;

/**
 * Class Employee
 * @author Karoline Sund Wahl
 * @version 1 (05.03.2021)
 */
public class Employee extends Person{

    /**
     * Constructor of employee objects.
     * Employee-objects are made to be implemented in list of employees in Department-class.
     * Calls on super (Person) to create object.
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * ToString()-method calls toString method from super(Person).
     * Subclasses of Employee use this toString, which really means that they all use Persons toString()-method.
     */
    @Override
    public String toString() {
        return super.toString();
    }
}


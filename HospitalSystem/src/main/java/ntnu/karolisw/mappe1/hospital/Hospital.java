package ntnu.karolisw.mappe1.hospital;

import java.util.ArrayList;

/**
 * Class Hospital
 * @author Karoline Sund Wahl
 * @version 1 (05.03.2021)
 */
public class Hospital {
    private final String hospitalName;
    private ArrayList<Department> departments;

    /**
     * Constructor which initializes list of departments
     * @param hospitalName
     */
    public Hospital(String hospitalName){
        this.departments = new ArrayList<>();
        this.hospitalName = hospitalName;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public ArrayList<Department> getDepartments(){
        return departments;
    }

    /**
     * Department is added to list of department with this method
     * Throws IllegalArgumentException (unchecked excpetion/runtime exeption) if department already exists
     * This is to avoid duplication
     * @param newDepartment
     */
    public void addDepartment(Department newDepartment){
        if(departments.contains(newDepartment)){
            throw new IllegalArgumentException();
        }
        departments.add(newDepartment);
    }

    /**
     * ToString()-method to print out Hospital name and departments
     * @return String of Hospital object
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Department department : departments){
            result.append(department.toString());
        }
        return "\nName of Hospital: " + hospitalName +
                "\n" + result;
    }
}

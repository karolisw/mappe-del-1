package ntnu.karolisw.mappe1.hospital;

/**
 * Abstract class Person
 * @author Karoline Sund Wahl
 * @version 1 (05.03.2021)
 */
public abstract class Person {
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    /**
     * This constructor has many subclasses who uses it
     * Uses exeption handling from mutator methods to avoid duplication.
     * Checks for null in arguments and letters in social security number
     * All fields to lower case and trimmed in mutator methods (for easier search mechanisms/implementations)
     * @throws IllegalArgumentException
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Person(String firstName, String lastName, String socialSecurityNumber) {
        setSocialSecurityNumber(socialSecurityNumber);
        setFirstName(firstName);
        setLastName(lastName);
    }


    /**
     * Accessor methods
     * @return fields
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFullName() {
        return firstName +" "+ lastName;
    }

    /**
     * Mutator-method that checks for letters or no input when setting an objects' social security number
     * @param socialSecurityNumber
     * @throws IllegalArgumentException
     */
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        if(socialSecurityNumber == null){
            throw new IllegalArgumentException("Social security number has to be empty String or String sequence of numbers");
        }
        char[] charArray = socialSecurityNumber.toLowerCase().toCharArray();
        for (char c : charArray) {
            if (c >= 'a' && c <= 'å') {
                throw new IllegalArgumentException("Social security number contained a letter");
            }
            this.socialSecurityNumber = socialSecurityNumber;
        }
    }

    /**
     * Mutator methods for first name
     * Checks for null in input
     * Trims input and puts it to lower case
     * @param firstName
     * @throws IllegalArgumentException
     */
        public void setFirstName (String firstName){
        if(firstName == null){
            throw new IllegalArgumentException("Firstname cannot be null");
        }
            this.firstName = firstName.trim().toLowerCase();
        }

    /**
     * Mutator method for last name with same functionality as method above
     * @param lastName
     * @throws IllegalArgumentException
     */
    public void setLastName (String lastName){
        if(lastName == null){
            throw new IllegalArgumentException("Lastname cannot be null");
        }
            this.lastName = lastName.trim().toLowerCase();
        }


    /**
     * Overridden toString()-method
     * Many subclasses derive this method
     * Converts all fields to Strings
     * @return String of Person object
     */
        @Override
        public String toString () {
            return "\nFirst Name: " + firstName +
                    "\nLast Name: " + lastName +
                    "\nSocial Security Number: " + socialSecurityNumber +"\n";
        }
}



package ntnu.karolisw.test;

import ntnu.karolisw.mappe1.exceptions.RemoveException;
import ntnu.karolisw.mappe1.hospital.Department;
import ntnu.karolisw.mappe1.hospital.Employee;
import ntnu.karolisw.mappe1.hospital.Patient;
import ntnu.karolisw.mappe1.hospital.Person;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test-class that tests removePerson()-method.
 * This class contains a "support"-class PatientTest to test protected Patient-objected
 *
 * Test class DepartmentTest
 * @author Karoline Sund Wahl
 * @version 1 (11.03.2021)
 */
public class DepartmentTest {


    /**
     * Testing to see what happens when we try to remove an employee who is not part of Department
     * (Negative testing)
     */
    @Test
    public void isEmployeeRemovable(){
        Department department = new Department("Hospital");
        Employee employee = new Employee("Karoline", "Wahl", "1234567890");
        try{
            department.removePerson(employee);
        } catch (RemoveException e) {
            e.printStackTrace();
        }
    }

    /**
     * Testing to see what happens when we try to remove an employee who is part of Department
     * (Positive testing)
     */
    @Test
    public void isEmployeeRemovable2(){
        Department department = new Department("Hospital");
        Employee employee = new Employee("Karoline", "Wahl", "1234567890");
        department.addEmployee(employee);
        try{
            department.removePerson(employee);
        } catch (RemoveException e) {
            e.printStackTrace();
        }
    }

    /**
     * Testing to see what happens when we try to remove a Patient who is part of Department
     * (Positive testing)
     */
    @Test
    public void isPatientRemovable(){
        Department department = new Department("Hospital");
        Patient patient = new PatientTest("Karoline", "Wahl", "1234567890");
        department.addPatient(patient);
        try{
            department.removePerson(patient);
        } catch (RemoveException e) {
            e.printStackTrace();
        }
    }

    /**
     * Testing to see what happens when we try to remove a patient who is not part of Department
     * (Negative testing)
     */
    @Test
    public void isPatientRemovable2(){
        Department department = new Department("Hospital");
        Patient patient = new PatientTest("Karoline", "Wahl", "1234567890");
        try{
            department.removePerson(patient);
        } catch (RemoveException e) {
            e.printStackTrace();
        }
    }


    /**
     * Support testing class to test Patient-object (part of removePerson()-method)
     * This has been done due to protected constructor in class Patient
     */
    public static class PatientTest extends Patient{

        public PatientTest(String firstName, String lastname, String socialSecurityNumber){
            super(firstName,lastname,socialSecurityNumber);
        }
    }


    /**
     * The following 4 tests are tests checking that the prerequisites
     * for using removePerson()-method is working well
     */

    @Test
    public void isEmployeeAddedToGetEmployees(){
        Department department = new Department("Hospital");
        Employee employee = new Employee("Karoline", "Wahl", "1234567890");
        department.addEmployee(employee);
        assertNotNull(department.getEmployees());
    }

    @Test
    public void isPatientAddedToGetPatients(){
        Department department = new Department("Hospital");
        PatientTest patientTest = new PatientTest("K", "KM", "12345678");
        department.addPatient(patientTest);
        assertNotNull(department.getPatients());
    }

    @Test
    public void isEmployeeSubclassOfPerson(){
        Employee employee = new Employee("Karoline", "Wahl", "1234567890");
        assertTrue(Person.class.isAssignableFrom(employee.getClass()));
    }

    @Test
    public void isPatientSubclassOfPerson() {
        assertTrue(Person.class.isAssignableFrom(Patient.class));
    }
    
}
